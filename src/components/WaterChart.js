import Chart from "react-apexcharts";
import React, { Component, useEffect, useState, async } from "react";

export default class WaterChart extends Component {
  constructor(props) {
    super(props);

    this.state = {
      options: {
        chart: {
          id: "apexchart-example",
        },
        xaxis: {
          categories: [
            "Jan",
            "Feb",
            "Mar",
            "Apr",
            "May",
            "Jun",
            "Jul",
            "Aug",
            "Sep",
            "Oct",
            "Nov",
            "Dec",
          ],
        },
      },
      series: [
        {
          name: "series-1",
          data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        },
      ],
      sensors: [],
    };
    this.simulate = this.simulate.bind(this);
    this.getSensors = this.getSensors.bind(this);
  }
  componentDidMount() {
    this.getSensors();
  }
  simulate() {
    var galons = 0;

    for (let i = 0; i < this.state.sensors.length; i++) {
      if (
        (this.state.sensors[i].open_close === 1) &
        (this.state.sensors[i].type === "Water")
      ) {
        console.log(galons);
        galons++;
      }
      this.setState({
        series: [
          {
            name: "series-1",
            data: [0, 0, 0, galons, 0, 0, 0, 0, 0, 0, 0, 0],
          },
        ],
      });
    }
  }

  getSensors = async () => {
    let response = await fetch("/api/sensors");
    let data = await response.json();
    this.setState({ sensors: data });
    console.log(data);
  };

  render() {
    return (
      <div>
      <h1 className="smart-home-list-item"> Water Consumption </h1>
        <Chart
          options={this.state.options}
          series={this.state.series}
          type="bar"
          width={500}
          height={320}
        />

        <button onClick={this.simulate}>{<h2>Simulate</h2>}</button>
      </div>
    );
  }
}
