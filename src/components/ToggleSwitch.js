import React from "react";

let getTitle = (light) => {
  let title = light.name;
  return title;
};

const ToggleSwitch = ({ light }) => {
  return (
    <div className="grid-container-toggle-grid dark">
      <div className="toggle">
        {getTitle(light)}
        <div className="toggle-switch">
          <input
            type="checkbox"
            className="checkbox"
            name={getTitle(light)}
            id={getTitle(light)}
          />
          <label className="label" htmlFor={getTitle(light)}>
            <span className="inner" />
            <span className="switch" />
          </label>
        </div>
      </div>
      </div>
  );
};

export default ToggleSwitch;
