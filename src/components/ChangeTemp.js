import React from "react";

function ChangeTemp() {
  var insideTemp = 70;
  var outsideTemp = 80;
  var lightBool = 0;
  
  function changeLights() {
    if (lightBool === 0) {
      alert("Turning Lights On");
      lightBool = 1;
    } else {
      alert("Turning Lights Off");
      lightBool = 0;
    }
  }

  function changeInsideTemperature() {
    insideTemp += 1;
    alert(insideTemp);
  }

  function changeOutsideTemperature() {
    outsideTemp += 1;
    alert(outsideTemp);
  }

  return (
    <div >
      <button onClick={() => changeInsideTemperature()}>
        <input type="number" defaultValue={insideTemp}></input>
        Change Interior Temp
      </button>
      <button onClick={() => changeOutsideTemperature()}>
        <input type="number" defaultValue={outsideTemp}></input>
        Change Exterior Temp
      </button>
      <button onClick={() => changeLights()}>
        Change Lights
      </button>
    </div>
  );
}

export default ChangeTemp;