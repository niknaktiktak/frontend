import React from "react";

const Header = () => {
  return (
    <div className="item1">
      <div className="app-header">
        <h1>Smart Home</h1>
      </div>
    </div>
  );
};

export default Header;
