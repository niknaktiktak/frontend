import "./App.css";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Header from "./components/Header";
import HomePage from "./pages/HomePage";
import React from "react";
import InfoPage from "./pages/InfoPage";
import ControlPage from "./pages/ControlPage"

function App() {
  return (
    <Router>
          <Route path="/" exact component={HomePage} />
          <Route path="/info" component={InfoPage}/>
          <Route path="/control" component={ControlPage}/>
    </Router>
  );
}

export default App;
